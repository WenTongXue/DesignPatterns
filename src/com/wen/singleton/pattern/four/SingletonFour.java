package com.wen.singleton.pattern.four;

public class SingletonFour {
    private volatile static SingletonFour singletonFour;
    private SingletonFour() {}

    public static SingletonFour getSingletonFour() {
        if (singletonFour == null) {
            synchronized (SingletonFour.class) {
                if (singletonFour == null) {
                    singletonFour = new SingletonFour();
                }
            }
        }
        return singletonFour;
    }

    public void showMessage(){
        System.out.println("Hello World Four!");
    }
}
