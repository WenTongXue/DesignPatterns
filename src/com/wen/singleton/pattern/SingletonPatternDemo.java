package com.wen.singleton.pattern;

import com.wen.singleton.pattern.five.SingletonFive;
import com.wen.singleton.pattern.four.SingletonFour;
import com.wen.singleton.pattern.one.SingletonOne;
import com.wen.singleton.pattern.six.SingletonSix;
import com.wen.singleton.pattern.three.SingletonThree;
import com.wen.singleton.pattern.two.SingletonTwo;
public class SingletonPatternDemo {
    public static void main(String[] args) {
        //不合法的构造函数
        //编译时错误：构造函数 Singleton() 是不可见的
        //Singleton object = new Singleton();

        //获取唯一可用的对象
        SingletonOne singletonOne = SingletonOne.getInstance();
        SingletonTwo singletonTwo = SingletonTwo.getInstance();
        SingletonThree singletonThree = SingletonThree.getInstance();
        SingletonFour singletonFour = SingletonFour.getSingletonFour();
        SingletonFive singletonFive = SingletonFive.getInstance();

        //显示消息
        singletonOne.showMessage();
        singletonTwo.showMessage();
        singletonThree.showMessage();
        singletonFour.showMessage();
        singletonFive.showMessage();
        SingletonSix.INSTANCE.whateverMethod();
    }
}
