package com.wen.decorator.pattern;

import com.wen.decorator.pattern.interfaces.Shape;

public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Shape: Rectangle");
    }
}
