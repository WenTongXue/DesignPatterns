package com.wen.decorator.pattern.interfaces;

public interface Shape {
    public void draw();
}
