package com.wen.decorator.pattern;

import com.wen.decorator.pattern.interfaces.Shape;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Shape: Circle");
    }
}
